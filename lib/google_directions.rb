# encoding: UTF-8
require 'cgi'
require 'net/http'
require 'open-uri'
require 'nokogiri'
require 'openssl'
require 'base64'

class GoogleDirections
  VERSION = '0.1.6.5'
  BASE_URL = 'http://maps.googleapis.com'
  BASE_PATH = '/maps/api/directions/json'
  DEFAULT_OPTIONS = {
      :language => :en,
      :alternative => :false,
      :sensor => :false,
      :mode => :driving
  }

  attr_reader :status, :json, :origin, :destination, :options, :route, :url, :total_duration, :total_distance, :waypoints

  def initialize(origin, destination, waypoints = nil, opts=DEFAULT_OPTIONS)
    @origin = origin
    @destination = destination
    @options = opts.merge({:origin => @origin, :destination => @destination})
    unless waypoints.nil?
      @options.merge!({:waypoints => 'optimize:true|' + waypoints.join('|')})
    end

    path = BASE_PATH + '?' + querify(@options)
    @url = BASE_URL + sign_path(path, @options)
    @json = JSON.parse(open(@url).read)

    @status = @json['status']
    @route = @json['routes'].first

    @total_duration = 0
    @total_distance = 0
    @waypoints = []
    leg_count = @route['legs'].size
    @route['legs'].each_with_index do |leg, index|
      @total_duration += leg['duration']['value']
      @total_distance += leg['distance']['value']

      if index != 1 && (index - 1) != leg_count
        @waypoints << leg['start_address']
        @waypoints << leg['end_address']
      end
    end

  end

  def waypoints_to_url
    points = ''
    @waypoints.each_with_index do |waypoint, index|
      points += transcribe(waypoint) + '+to:'
    end
    points
  end

  def total_duration_in_minutes
    seconds_to_minutes(@total_duration)
  end

  def total_duration_in_minutes_text
    seconds_to_minutes(@total_duration).to_s + ' minutes'
  end

  def total_distance_text
    m_to_km(@total_distance).to_s + ' km'
  end


  def successful?
    @status == 'OK'
  end

  def public_url
    "http://maps.google.com/maps?saddr=#{transcribe(@origin)}&daddr=#{waypoints_to_url}#{transcribe(@destination)}&hl=#{@options[:language]}&ie=UTF8"
    # "http://maps.google.com/maps?saddr=#{transcribe(@origin)}&daddr=#{transcribe(@destination)}&hl=#{@options[:language]}&ie=UTF8"
  end


  def steps
    if successful?
      if @steps.nil?
        @steps = []
        @route['legs'].each do |leg|
          leg['steps'].each do |step|
            @steps << step['html_instructions']
          end
        end
      end
      @steps
    else
      []
    end
  end

  private

  def m_to_km(value)
    (value / 1000.0).round(3)
  end

  def seconds_to_minutes(value)
    (value / 60).round
  end

  def transcribe(location)
    CGI::escape(location)
  end

  def querify(options)
    params = []

    options.each do |k, v|
      params << "#{transcribe(k.to_s)}=#{transcribe(v.to_s)}" unless k == :private_key
    end

    params.join('&')
  end

  def sign_path(path, options)
    return path unless options[:private_key]

    raw_private_key = url_safe_base64_decode(options[:private_key])
    digest = OpenSSL::Digest.new('sha1')
    raw_signature = OpenSSL::HMAC.digest(digest, raw_private_key, path)
    path + "&signature=#{url_safe_base64_encode(raw_signature)}"
  end

  def url_safe_base64_decode(base64_string)
    Base64.decode64(base64_string.tr('-_', '+/'))
  end

  def url_safe_base64_encode(raw)
    Base64.encode64(raw).tr('+/', '-_').strip
  end

end



